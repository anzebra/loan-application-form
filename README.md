Loan app
--------------

### Installation
Clone the project and run next commands:

```sh
$ composer install
$ php bin/console assetic:dump
$ php bin/console doctrine:schema:create
```

During installation set your application url or just add it into file app/parameters.yml:
```sh
 app_url: http://your_app.local
```

### Documentation
Documentation will be available by the following url:
```sh
 http://your_app.local/api/doc
```


### API methods
```sh
 GET /api/loans - returns list of loans
```

```sh
 POST /api/loans - creates new loan.

 Request body example:
 {
 	"amount": "300",
    "property_value": "10500",
    "social_security": "078051120"
 }
```

