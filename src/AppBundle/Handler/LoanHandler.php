<?php
namespace AppBundle\Handler;

use AppBundle\Entity\Loan;
use AppBundle\Form\Handler\FormHandlerInterface;
use AppBundle\Repository\LoanRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoanHandler
 * @package AppBundle\Handler
 */
class LoanHandler implements HandlerInterface
{
    /**
     * @var FormHandlerInterface
     */
    private $formHandler;
    
    /**
     * @var LoanRepository
     */
    private $repository;

    /**
     * LoanHandler constructor.
     * @param FormHandlerInterface $formHandler
     * @param LoanRepository       $loanRepository
     */
    public function __construct(
        FormHandlerInterface $formHandler,
        LoanRepository $loanRepository
    )
    {
        $this->formHandler = $formHandler;
        $this->repository = $loanRepository;
    }
    /**
     * @return mixed
     */
    public function all()
    {
        return $this->repository->findAll();
    }
    /**
     * @param array                 $parameters
     * @param array                 $options
     * @return Loan
     */
    public function post(array $parameters, array $options = [])
    {
        $loan = $this->formHandler->handle(
            new Loan(),
            $parameters,
            Request::METHOD_POST,
            $options
        );
        
        $this->repository->save($loan);
        
        return $loan;
    }
}