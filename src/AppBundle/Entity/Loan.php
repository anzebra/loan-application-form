<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Loan
 *
 * @ORM\Table(name="loan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LoanRepository")
 *
 * @ExclusionPolicy("all")
 */
class Loan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     * @Assert\NotBlank(message="Field 'Amount' is required.")
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(name="property_value", type="integer")
     * @Assert\NotBlank(message="Field 'Property value' is required.")
     */
    private $propertyValue;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security", type="string", length=13)
     * @Assert\NotBlank(message="Field 'SSN' is required.")
     * @Assert\Length(
     *      min = 9,
     *      max = 13,
     *      minMessage = "Your SSN must be at least {{ limit }} characters long",
     *      maxMessage = "Your SSN cannot be longer than {{ limit }} characters"
     * )
     *
     */
    private $socialSecurity;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     * @Expose
     */
    private $status = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Loan
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set propertyValue
     *
     * @param integer $propertyValue
     *
     * @return Loan
     */
    public function setPropertyValue($propertyValue)
    {
        $this->propertyValue = $propertyValue;

        return $this;
    }

    /**
     * Get propertyValue
     *
     * @return int
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * Set socialSecurity
     *
     * @param string $socialSecurity
     *
     * @return Loan
     */
    public function setSocialSecurity($socialSecurity)
    {
        $this->socialSecurity = $socialSecurity;

        return $this;
    }

    /**
     * Get socialSecurity
     *
     * @return string
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Loan
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }


}

