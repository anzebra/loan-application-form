<?php
namespace AppBundle\Controller\Api;

use AppBundle\Entity\Loan;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Exception\InvalidFormException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class LoanController
 */
class LoanController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Gets a collection of the Loans.
     *
     * @ApiDoc(
     *   output = "AppBundle\Entity\Loan",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @throws NotFoundHttpException when does not exist
     *
     * @return array|View
     */
    public function cgetAction()
    {
        return $this->getHandler()->all();
    }

    /**
     * Creates a new Loan.
     *
     * @ApiDoc(
     *  input = "AppBundle\Form\Type\LoanFormType",
     *  output = "AppBundle\Entity\Loan",
     *  statusCodes={
     *         200="Returned when a new Loan has been successfully created",
     *         400="Returned when the posted data is invalid"
     *     }
     * )
     *
     * @param Request $request
     * @return Loan|View
     */
    public function postAction(Request $request)
    {
        try {
            return $this->getHandler()->post($request->request->all());
        } catch (InvalidFormException $e) {
            return new JsonResponse(['errors' => $e->getFormErrors()], 400);
        }
    }

    /**
     * Returns the required handler for this controller
     *
     * @return \AppBundle\Handler\LoanHandler
     */
    private function getHandler()
    {
        return $this->get('appbundle.handler.loan_handler');
    }
}