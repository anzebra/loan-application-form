<?php
namespace AppBundle\Form\Type;
use AppBundle\Entity\Loan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class LoanFormType
 * @package AppBundle\Form\Type
 */
class LoanFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', NumberType::class, ['invalid_message' => "'Amount' is required integer field."])
            ->add('property_value', NumberType::class, ['invalid_message' => "'Property value' is required integer field."])
            ->add('social_security', TextType::class)
        ;
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Loan::class
        ]);
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'loan';
    }
}