<?php
namespace AppBundle\Exception;
use Symfony\Component\Form\FormInterface;

/**
 * Class InvalidFormException
 * @package AppBundle\Exception
 */
class InvalidFormException extends \RuntimeException
{
    const DEFAULT_ERROR_MESSAGE = "The submitted data was invalid.";

    protected $formErrors = [];

    /**
     * @param null $form
     * @param string $message
     */
    public function __construct($form = null, $message = self::DEFAULT_ERROR_MESSAGE)
    {
        parent::__construct($message);
        $this->formErrors = $this->getErrorsFromForm($form);
    }


    public function getFormErrors()
    {
        return $this->formErrors;
    }
    
    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors = array_merge($errors, $childErrors);
                }
            }
        }
        
        return $errors;
    }
}