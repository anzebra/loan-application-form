/**
 * Main AngularJS Web Application
 */
var app = angular.module('tutorialWebApp', [
    'ngRoute'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
    // Home
        .when("/", {templateUrl: "/partials/home.html", controller: "PageCtrl"});
}]);

/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function ($scope, $http) {
    $scope.loans = [];
    $scope.formData = {};
    $scope.loansShowEmptyMessage = false;
    $scope.formErrors = [];
    // when landing on the page, get all Loans and show them
    $http.get(API_URL + '/loans').then(function (request) {
        $scope.loans = request.data;
        $scope.loansShowEmptyMessage = $scope.loans.length == 0;
    });

    // when submitting the add form, send the Loan to the node API
    $scope.loanAdd = function() {
        $http.post(API_URL + '/loans', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.loans.push(data);
                $scope.formErrors = [];
                console.log(data);
            })
            .error(function(data) {
                $scope.formErrors = [];
                if (data.errors) {
                    $scope.formErrors = data.errors;
                }
            });
    };
});